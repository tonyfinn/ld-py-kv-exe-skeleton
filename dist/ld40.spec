# -*- mode: python -*-

from kivy.deps import sdl2, glew

import os

block_cipher = None

a = Analysis(['..\\ld40\\__main__.py'],
             pathex=[],
             binaries=[],
             datas=[('..\\assets\\*.png', 'assets')],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          Tree('..\\ld40\\'),
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          *[Tree(p) for p in (sdl2.dep_bins + glew.dep_bins)],
          name='ld40',
          debug=False,
          strip=False,
          upx=True,
          console=True )
