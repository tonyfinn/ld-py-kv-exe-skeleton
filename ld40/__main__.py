import kivy
kivy.require('1.9.1')

from kivy.app import App
from kivy.core.window import Window
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.label import Label

class LD40(App):
    def build(self):
        self.layout = FloatLayout()
        ld40 = 'Ludum 40 Skeleton'
        label = Label(text=ld40, height='50sp', width=200)
        self.layout.add_widget(label)
        return self.layout


if __name__ == '__main__':
    Window.size = (1024, 768)
    LD40().run()