from distutils.core import setup

setup(
    name='LD40',
    version='0.1',
    packages=['ld40'],
    long_description=open('README.md').read(),
    include_package_data=True
)